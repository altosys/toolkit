version: '3'

env:
  CGO_ENABLED: 0

vars:
  _PROJECT: '{{ default ._DEFAULT_PROJECT .PROJECT }}'
  _DEFAULT_PROJECT:
    sh: git config --get remote.origin.url | awk -F"/" '{print $NF}' | sed 's/\.[^.]*$//'
  _BUILD_DEST: '{{ default (print "bin/" .PROJECT) .BUILD }}'
  _BUILD_SRC: '{{ default "./..." .BUILD_SRC }}'
  _BUILDER: '{{ default .USER .BUILDER }}'
  _DATE:
    sh: date
  _GIT_COMMIT:
    sh: git rev-parse --short=8 HEAD
  _GIT_VERSION:
    sh: git tag -l --points-at HEAD | grep -E "^v([0-9]+\.)([0-9]+\.)([0-9]+)(-[a-z]+\.[0-9])?" || echo "n/a"
  _VERBOSE: '{{ if or (eq (env "V") "true") (eq (env "V") "1") }}-v{{ end }}'

tasks:
  default:
    cmds:
      - task: :list

  build:
    desc: build project binary
    cmds: &gocompile
      - >
        GOARCH={{ARCH}} GOOS={{OS}} go build -o {{.BUILD_DEST}} -v
        -ldflags="-w -s
        -X '{{._PROJECT}}.Builder={{._BUILDER}}'
        -X '{{._PROJECT}}.Commit={{._GIT_COMMIT}}'
        -X '{{._PROJECT}}.Date={{._DATE}}'
        -X '{{._PROJECT}}.Version={{ default ._GIT_VERSION .VERSION }}'"
        {{._BUILD_SRC}}
    generates:
      - '{{._BUILD_DEST}}'

  install:
    desc: build and install project binary to GOPATH
    cmds: *gocompile
    vars:
      _CMD: install
      _OUTPUT: ''

  lint:
    desc: run golangci-lint static analysis
    cmds:
      - golangci-lint run
    preconditions: &lint-precon
      - sh: which golangci-lint
        msg: |
          golangci-lint not found, install to continue

  fix:
    desc: attempt to fix static code errors
    cmds:
      - golangci-lint run ./... --fix -E godox -E gofmt -E misspell
    preconditions: *lint-precon

  bench:
    desc: run go benchmark tests
    cmds:
      - go test -bench={{ catLines ._GOPKGS }}
    vars:
      _GOPKGS:
        sh: go list ./...

  unit:
    desc: run go unit tests
    cmds:
      - go test -count=1 -short ./... {{ ._VERBOSE }}

  intg:
    desc: run go integration tests
    summary: |
      Run go integration tests

      VARIABLES:
        "V" (0|false or 1|true) - Set V=1 to run tests with verbose output
    cmds:
      - go test ./... -run Integration -count 1 {{ ._VERBOSE }}
    silent: true

  cover:
    desc: run go tests with coverage report
    summary: |
      Measure coverage of unit and integration tests

      VARIABLES:
        "V" (0|false or 1|true) - Set V=1 to run tests with verbose output
    cmds:
      - go test ./... -count 1 -coverprofile=cover.out {{ ._VERBOSE }}
      - go tool cover -html=cover.out -o cover.html
      - go tool cover -func=cover.out
    silent: true

  test:
    desc: run go tests
    summary: |
      Run go unit and integration tests

      VARIABLES:
        "V" (0|false or 1|true) - Set V=1 to run tests with verbose output
    cmds:
      - go test ./... -count 1 {{ ._VERBOSE }}
    silent: true

  race:
    desc: run go race condition tests
    summary: |
      Run go tests with race condition detector enabled

      VARIABLES:
        "V" (0|false or 1|true) - Set V=1 to run tests with verbose output
    vars:
      _REPEAT: '{{ default 1 .REPEAT }}'
    env:
      CGO_ENABLED: 1
    cmds:
      - go test -count={{ default 1 ._REPEAT }} -race -parallel 1 ./... {{ ._VERBOSE }}
    silent: true

  releaser:
    desc: run goreleaser
    cmds:
      - >
        goreleaser release --rm-dist
        {{ if .DRY_RUN }}--skip-publish{{ end }}
        {{ if .DRY_RUN }}--skip-validate{{ end }}
        {{ if .RELEASE_NOTES }}--release-notes <(git-chglog $(svu current)){{ end }}
    env:
      BASE_IMAGE: '{{ .BASE_IMAGE }}'
    preconditions:
      - sh: '[ "$CI" = "true" ]'
        msg: 'task go:releaser is only allowed to run in CI environment'
      - sh: which goreleaser
        msg: 'goreleaser not found, install to continue'
      - sh: 'if [ "$RELEASE_NOTES" != "" ]; then which svu; fi'
        msg: 'svu not found, install to continue'
      - sh: 'if [ "$RELEASE_NOTES" != "" ]; then which git-chglog; fi'
        msg: 'git-chglog not found, install to continue'

  pkgs:
    desc: list go packages in project
    cmds:
      - echo '{{ ._GOPKGS }}'
    vars:
      _GOPKGS:
        sh: go list ./...
    silent: true
