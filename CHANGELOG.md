# Changelog - toolkit

## [1.2.1](https://gitlab.com/altosys/toolkit/compare/v1.2.0...v1.2.1) (2022-08-25)


### Bug Fixes

* **podman:** only tag build with project name ([1278651](https://gitlab.com/altosys/toolkit/commit/1278651f092653ce16e4832ee3cd53af80324e36))

# [1.2.0](https://gitlab.com/altosys/toolkit/compare/v1.1.1...v1.2.0) (2022-08-24)


### Features

* **go:** add VERSION as input to go:build with fallback to _GIT_VERSION ([1169062](https://gitlab.com/altosys/toolkit/commit/1169062f1dc29747c684c1d9131ae10eacaa4958))

## [1.1.1](https://gitlab.com/altosys/toolkit/compare/v1.1.0...v1.1.1) (2022-08-23)


### Bug Fixes

* **go:** make goreleaser release notes flag conditional ([abaf29e](https://gitlab.com/altosys/toolkit/commit/abaf29e53ad1e04b0106956015e812a3ef8c004d))

# [1.1.0](https://gitlab.com/altosys/toolkit/compare/v1.0.23...v1.1.0) (2022-08-12)


### Features

* switch from docker to podman ([fde8af6](https://gitlab.com/altosys/toolkit/commit/fde8af67b272f4694afc3056be36bee6d34bc0f8))

Changelog - toolkit

## [1.0.23](https://gitlab.com/altosys/toolkit/compare/v1.0.22...v1.0.23) (2021-04-18)


### Bug Fixes

* **go:** use project base package for metadata ldflags instead of 'meta' package ([76e0fad](https://gitlab.com/altosys/toolkit/commit/76e0fada5b026c014d41c874ee5daac8f75106b5))

## [1.0.22](https://gitlab.com/altosys/toolkit/compare/v1.0.21...v1.0.22) (2021-04-11)


### Bug Fixes

* disable goreleaser '--skip-validate' flag if not DRY_RUN=true ([86ab0e7](https://gitlab.com/altosys/toolkit/commit/86ab0e71910616b39575c52f067aeaa0f9819a26))

## [1.0.21](https://gitlab.com/altosys/toolkit/compare/v1.0.20...v1.0.21) (2021-04-10)


### Bug Fixes

* update goreleaser support in taskfiles for docker and go ([6135f10](https://gitlab.com/altosys/toolkit/commit/6135f10179acfaf6b52ca064ff5acedc394c6ef6))

## [1.0.20](https://gitlab.com/altosys/toolkit/compare/v1.0.19...v1.0.20) (2021-04-04)


### Bug Fixes

* update golangci-lint 'go:static' to 'go:lint' and remove 'go:dox' ([39bcbe4](https://gitlab.com/altosys/toolkit/commit/39bcbe476e860d79d435eaa51010e4e670205be0))

## [1.0.19](https://gitlab.com/altosys/toolkit/compare/v1.0.18...v1.0.19) (2021-04-02)


### Bug Fixes

* deprecate running go tasks in docker-compose ([97dc09a](https://gitlab.com/altosys/toolkit/commit/97dc09a47db284e38b307a55fd89dda82db934fa))

## [1.0.18](https://gitlab.com/altosys/toolkit/compare/v1.0.17...v1.0.18) (2021-03-27)


### Bug Fixes

* update go build task to work with go 1.16 ([ffae982](https://gitlab.com/altosys/toolkit/commit/ffae982dedef94344b1ce23e744bf2c321ec4172))

## [1.0.17](https://gitlab.com/altosys/toolkit/compare/v1.0.16...v1.0.17) (2021-01-08)


### Bug Fixes

* **go:** add golint to go:static linters ([cb33fc4](https://gitlab.com/altosys/toolkit/commit/cb33fc4ca9df980e3e9357a3745a5bc4af5a21e6))

## [1.0.16](https://gitlab.com/altosys/toolkit/compare/v1.0.15...v1.0.16) (2021-01-04)


### Bug Fixes

* add explicit DOCKER_REGISTRY to all docker image references ([c69dae0](https://gitlab.com/altosys/toolkit/commit/c69dae037dd4496d1ccb2cfc635218a6a3500e12))

## [1.0.15](https://gitlab.com/altosys/toolkit/compare/v1.0.14...v1.0.15) (2021-01-03)


### Bug Fixes

* update go race test image to buster base ([e7472bf](https://gitlab.com/altosys/toolkit/commit/e7472bfd0b33f081e6a4a5955b088808b1e5ab4d))

## [1.0.14](https://gitlab.com/altosys/toolkit/compare/v1.0.13...v1.0.14) (2020-11-28)


### Bug Fixes

* improve go tasks use of CI and V variables, disable verbose test output by default ([e7b5473](https://gitlab.com/altosys/toolkit/commit/e7b54738eeea4165a6cab5321ad10937655c5373))

## [1.0.13](https://gitlab.com/altosys/toolkit/compare/v1.0.12...v1.0.13) (2020-10-19)


### Bug Fixes

* add docker image tag without registry path for pushing to docker hub ([c80de5b](https://gitlab.com/altosys/toolkit/commit/c80de5b263039da843b5f425fbcf453e818a008d))

## [1.0.12](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.11...v1.0.12) (2020-09-19)


### Bug Fixes

* update go-task to v3.0.0 ([55943b9](https://gitlab.com/tommy.alatalo/toolkit/commit/55943b9896d4e97f65791aff9bb890cdbce0b000))

## [1.0.11](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.10...v1.0.11) (2020-09-19)


### Bug Fixes

* improve docker tasks and yaml formatting ([55dd056](https://gitlab.com/tommy.alatalo/toolkit/commit/55dd0564ebb28c0adfb3e15d70b08a65f7289d15))

## [1.0.10](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.9...v1.0.10) (2020-08-25)


### Bug Fixes

* move go version and docker image definitions to parent taskfile ([1766bd2](https://gitlab.com/tommy.alatalo/toolkit/commit/1766bd2d5530c8f9f26749d190831917083df596))

## [1.0.9](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.8...v1.0.9) (2020-08-15)


### Bug Fixes

* **docker:** correction for project path in tags for docker:build ([ae10230](https://gitlab.com/tommy.alatalo/toolkit/commit/ae1023009fe0179a9ff66a630017e9fb6b589a10))

## [1.0.8](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.7...v1.0.8) (2020-08-05)


### Bug Fixes

* **docker:** update quoting around env vars passed as build args ([52eefa9](https://gitlab.com/tommy.alatalo/toolkit/commit/52eefa95bca72993bda042188bad484e4edc1ade))

## [1.0.7](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.6...v1.0.7) (2020-08-04)


### Bug Fixes

* **go:** change metadata package from 'cmd' to 'meta' ([19722dd](https://gitlab.com/tommy.alatalo/toolkit/commit/19722ddc26054395ebd8956d6bc8a225b7b2f9d3))

## [1.0.6](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.5...v1.0.6) (2020-07-07)


### Bug Fixes

* **go:** set test run max parallel=1 to avoid interference ([ab253c1](https://gitlab.com/tommy.alatalo/toolkit/commit/ab253c1d4bd2cbcf48e4dd4e63cb25f0036b5f9e))

## [1.0.5](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.4...v1.0.5) (2020-07-06)


### Bug Fixes

* **go:** disable cache on test tasks ([eb34c41](https://gitlab.com/tommy.alatalo/toolkit/commit/eb34c4155d841cb8b022516979380c49c54f6656))

## [1.0.4](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.3...v1.0.4) (2020-07-04)


### Bug Fixes

* **go:** set race tests to run sequentially and add REPEAT var to task ([3c031df](https://gitlab.com/tommy.alatalo/toolkit/commit/3c031dfef7ff0002d5c73377e0e737d01bee60fa))

## [1.0.3](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.2...v1.0.3) (2020-04-14)


### Bug Fixes

* add COVERAGE variable to gitlab init task ([5f32a4c](https://gitlab.com/tommy.alatalo/toolkit/commit/5f32a4c1c1517a241b809e412f2b338a3784e855))

## [1.0.2](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.1...v1.0.2) (2020-03-18)


### Bug Fixes

* **go:** add --abort-on-container-exit flag to remove warning from dockerized go tests ([0d25cf5](https://gitlab.com/tommy.alatalo/toolkit/commit/0d25cf5c258f3cc8b4a9777292d3af99e36d7260))

## [1.0.1](https://gitlab.com/tommy.alatalo/toolkit/compare/v1.0.0...v1.0.1) (2020-03-18)


### Bug Fixes

* **go:** remove faulty ignore_error setting on go intg and race tests ([e999a44](https://gitlab.com/tommy.alatalo/toolkit/commit/e999a4475ac91995c5604f1b9f1d78ed972a0036))

# 1.0.0 (2020-02-27)


### Features

* set up initial tools ([8240a42](https://gitlab.com/tommy.alatalo/toolkit/commit/8240a42b0a4d2c4b75923f67f0b77dc5b9e05cd2))
